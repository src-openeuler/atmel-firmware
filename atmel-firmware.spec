Name:           atmel-firmware
Version:        1.3
Release:        21
Summary:        Firmware for Atmel at76c50x Wireless Network Chips
License:        GPL-2.0-or-later
URL:            http://at76c503a.berlios.de/
Source0:        http://www.thekelleys.org.uk/atmel/atmel-firmware-%{version}.tar.gz
Source1:        http://download.berlios.de/at76c503a/at76_usb-firmware-0.1.tar.gz
BuildArch:      noarch
Recommends:     %{name}-help

Obsoletes:      at76_usb-firmware < 0.1
Provides:       at76_usb-firmware = 0.1

%description
The drivers for Atmel at76c50x wireless network chips in the Linux 2.6.x kernel
and at http://at76c503a.berlios.de/ do not include the firmware and this
firmware needs to be loaded by the host on most cards using these chips.

This package provides the firmware images that should be automatically loaded
as needed by the hotplug system. It also provides a small loader utility that
can be used to accomplish the same thing when hotplug is not in use.

%package_help

%prep
%autosetup -n %{name}-%{version}
%setup  -q -D -T -a 1 
install -pm 0644 at76_usb-firmware-0.1/COPYRIGHT COPYRIGHT-usb
install -pm 0644 at76_usb-firmware-0.1/README README-usb
for i in COPYRIGHT-usb README-usb COPYING README; do
install -pm 0644 ${i} ${i}.atmel-firmware
rm  ${i}
ln -sf /lib/firmware/${i}.atmel-firmware ${i}
done

%build

%install
install -d  %{buildroot}/lib/firmware
install -p  images/*.bin  %{buildroot}/lib/firmware
install -p  at76_usb-firmware-0.1/*.bin %{buildroot}/lib/firmware
install -p  *.atmel-firmware  %{buildroot}/lib/firmware

%files
%doc COPYING COPYRIGHT-usb
%attr(0644,root,root) /lib/firmware/*.bin  
%attr(0644,root,root) /lib/firmware/*.atmel-firmware

%files help
%doc VERSION README README-usb

%changelog
* Fri Jun 14 2024 xinghe <xinghe2@h-partners.com> - 1.3-21
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix license and fix install error

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3-20
- Package init

